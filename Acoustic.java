/**
* Acoustic class
*
* a class to represent acoustic guitars
*
*/
class Acoustic extends Guitar {
    public Acoustic(String type){
        super(type);
    }
}
