/**
* Main class
*
* run example, includes basics of classes and objects
*
* CLASS
*
* A set of items identified by one or more properties.
*
* OBJECT
*
* An item, element of a class or set.
*
* COMPILE
*
* javac Main.java
*
* RUN
*
* java Main
*
* TEST ENVIRONMENT
*
* os lubuntu 16.04 javac version 11.0.5
*
* EXAMPLE OUTPUT
*
* Guitar A Acoustic
* Guitar B Bass
*
*/
class Main{
    public static void main(String[] args){
        Guitar a = new Acoustic("Acoustic");
        Guitar b = new Bass("Bass");
        System.out.println("Guitar A "+a.type);
        System.out.println("Guitar B "+b.type);
    }
}
