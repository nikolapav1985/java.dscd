/**
* Bass class
*
* a class to represent bass guitars
*
*/
class Bass extends Guitar {
    public Bass(String type){ // initialize bass object
        super(type);
    }
}
