/**
* Guitar class
*
* a generic class to represent different types of guitars
*
*/
class Guitar {
    public String type;

    public Guitar(String type){ // initialize guitar object
        this.type=type;
    }
}
